<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-theme');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'admin');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Km3;~9g#iT4;PeJ{dBW_kr/Vj-S81A^:~gWg`i:bU#v+2fXJ:3G;[Xr08<g:$^UY');
define('SECURE_AUTH_KEY',  '[$ZY&0BRn=*@`T^c+-ST[!9sF)o?:ak/Tx_QI}vQQaMm8gRc2Di?|t+R|9@.,c3b');
define('LOGGED_IN_KEY',    '>1H;cqq-4Ns[;xvGePE6/S/.j;VxU|>@5qunHh}T{x.k=TrycI[uZ-nU?Evcqo||');
define('NONCE_KEY',        ';fRcC=c:Kf5oaY#l%B!(hPI7rQYj]jt[2u9e$:US|Se6^RF@GYB{LCd.&.c&3HOT');
define('AUTH_SALT',        'x>*e|wKO58V])?z_*i!8?pyyp5]i]e~$6X]+~Yt?IF8fGGE}0a_(n,k@/a,hdy*3');
define('SECURE_AUTH_SALT', 'p[s2|*E`_Z=QuLHxDG.?,-5>g*A?Vj%vvW}].xuIR`$Oykr_OJ|XrPVoXIt$>5Zt');
define('LOGGED_IN_SALT',   'Bt0{>DGzw*wk TkT4O|6{;t-L7cjxElc){EpO,,w-E<{^1(w|/Ue~NFPWEOCn^;r');
define('NONCE_SALT',       '`xh$a )NT,qr^}@_>Y?u!W9fS4Ug%xQdSxMeG1GFCDK[!3TunUJ4;v}mg~|l=^oa');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);

// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define( 'SCRIPT_DEBUG', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
