<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">



    <div class="text-center">	
        <div class="content singledefult">
          
            <!-- <div class="blog-category"><em><a href="http://everly.premiumcoding.com/category/decoration/" >Decoration</a>, <a href="http://everly.premiumcoding.com/category/editors-choice/" >Editor's choice</a></em> </div> -->

            <header class="entry-header">
                <?php the_title('<h1 class="entry-title">', '</h1>'); ?>

                <div class="entry-meta">

                    <?php understrap_posted_on(); ?>

                </div><!-- .entry-meta -->

            </header><!-- .entry-header -->

            <!--  <div class = "post-meta">
                <a class="post-meta-time" href="http://everly.premiumcoding.com/2016/03/13/">March 13, 2016</a> | <a class="post-meta-author" href="">by Everly</a> | <a href="http://everly.premiumcoding.com/2016/03/13/plants-will-give-energy-to-the-room/#commentform">No Comments</a>				
            </div> -->
            <!-- end of post meta -->
        </div>			

        <?php echo get_the_post_thumbnail($post->ID, 'large'); ?>

    </div>



    <div class="entry-content">

        <?php the_content(); ?>

        <?php
        wp_link_pages(array(
            'before' => '<div class="page-links">' . __('Pages:', 'understrap'),
            'after' => '</div>',
        ));
        ?>

    </div><!-- .entry-content -->

    <footer class="entry-footer">

        <?php understrap_entry_footer(); ?>

    </footer><!-- .entry-footer -->

</article><!-- #post-## -->
