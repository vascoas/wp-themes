= Installation

Requirements:
* must have gulp installed (npm install gulp -g )

* git clone this project

* Create a database and run mysql admin
** db name: wp-theme
** collation: utf8_general_ci
** user: root
*** grant all permissions to the db
*** user host: localhost
** pass: '' (empty / no password)

* Go to http://localhost:8888/
** Install Wordpress
** username: admin
** password: admin

* Login in wordpress
** Go to Plugins
** Activate all plugins

* In the WP left menu, select Appearance
** Activate the UnderStrap Child

* In the WP left menu, select FakerPress
** Set a quantity (eg. 3 to 12)
** Hit Generate

* Go to wp-content > themes > understrap-child
** Run: npm install
** Run: gulp watch


= Wordpress User
user: admin
pass: admin
